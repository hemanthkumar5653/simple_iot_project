#include<math.h>
#include<Servo.h>
#include <SoftwareSerial.h>
#include <Keypad.h>
#include<math.h>
const byte ROWS = 4; //four rows
const byte COLS = 3; //three columns
char keys[ROWS][COLS] = {
    {'1','2','3'},
    {'4','5','6'},
    {'7','8','9'},
    {'*','0','#'}
};

byte rowPins[ROWS] = {53, 51, 49, 47}; //connect to the row pinouts of the keypad
byte colPins[COLS] = {45, 43, 41}; //connect to the column pinouts of the keypad
int v1,v2,v4;
double v3;
// initialize the library with the numbers of the interface pins
Keypad keypad = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS );

SoftwareSerial mySerial(9,10);
char message;
Servo disc,holder,shoulder,base;

//motor one
int enA=42;
int in1= 44;
int in2= 46;

//motor two
int enB=52;
int in3=48;
int in4=50;

//motor one(Collector)
int enA1=28;
int in11= 30;
int in21= 32;

//motor two(Driller)
int enB1=38;
int in31=34;
int in41=36;

//relay
int water = 20;
int drill= 21;

void setup() {
  
pinMode(enA,OUTPUT);
pinMode(enB,OUTPUT);
pinMode(in1,OUTPUT);
pinMode(in2,OUTPUT);
pinMode(in3,OUTPUT);
pinMode(in4,OUTPUT);
pinMode(enA1,OUTPUT);
pinMode(enB1,OUTPUT);
pinMode(in11,OUTPUT);
pinMode(in21,OUTPUT);
pinMode(in31,OUTPUT);
pinMode(in41,OUTPUT);

// Turn off motors - Initial state
 digitalWrite(in1, LOW);
 digitalWrite(in2, LOW);
 digitalWrite(in3, LOW);
 digitalWrite(in4, LOW);
 digitalWrite(in11, LOW);
 digitalWrite(in21, LOW);
 digitalWrite(in31, LOW);
 digitalWrite(in41, LOW);

 disc.attach(13);
 holder.attach(8);
 shoulder.attach(7);
 base.attach(6);
 disc.write(0);
 holder.write(0);
 shoulder.write(0);
 base.write(0);

 pinMode(water, OUTPUT);
 pinMode(drill, OUTPUT);
 digitalWrite(water,LOW);
 digitalWrite(drill,LOW);
 Serial.begin(9600);
 mySerial.begin(9600);

}

void waterPump(){
  
  digitalWrite(water,HIGH);
  delay(500);
  digitalWrite(water,LOW);
  delay(100);
}

void Start(){
  
  //this function will run the motors in both directions at a fixed speed
  //turn on motor A          
  digitalWrite(in1,LOW);
  digitalWrite(in2,HIGH);
  //set the speed to 200 out of possible range 0~255
  analogWrite(enA,200);
  //turn on motor B
   digitalWrite(in3,HIGH);
  digitalWrite(in4,LOW);
    //set the speed to 200 out of possible range 0~255
  analogWrite(enB,200);
  
}

void roboticArmContract(){
  
 shoulder.write(90); //side arm
 delay(100);
 holder.write(90); //holder contracts
 delay(100);
 holder.write(30); //holder holds the sensor tube
 delay(100);
 shoulder.write(0); //comes back to the original state
 delay(100);
 base.write(90); //base part rotates towards solution
 delay(100);

} 

 void roboticArmBack(){
  
 shoulder.write(90); //side arm goes down
 delay(500);
 shoulder.write(0); //side arm goes up
 delay(100);
 base.write(0); //base arm comes back 
 delay(100);
 shoulder.write(90); //side arm goes down
 delay(100);
 holder.write(90); //holder contracts back
 delay(100);
 shoulder.write(0); //side arm comes up
 delay(100);
 shoulder.write(0); //side arm closses
 delay(100);

}

void Stop(){

  //setting motors to shut down stage
  digitalWrite(in1,LOW);
  digitalWrite(in2,LOW);
  digitalWrite(in3,LOW);
  digitalWrite(in4,LOW);
  
}

void StartCollector(){
  
  //this function will run the motors in both directions at a fixed speed
  //turn on motor A
  digitalWrite(in11,HIGH);
  digitalWrite(in21,LOW);
  //set the speed to 200 out of possible range 0~255
  analogWrite(enA1,200);
  delay(300);
  digitalWrite(in11,LOW); //anticlockwise rotation
  digitalWrite(in21,HIGH);
  delay(300);
  digitalWrite(in11,LOW); //off the collector
  digitalWrite(in21,LOW);
  delay(100);
}

void StartDriller(){

  //this function will run the motors in both directions at a fixed speed
  //turn on motor B
  digitalWrite(in31,LOW);
  digitalWrite(in41,HIGH);
  //set the speed to 200 out of possible range 0~255
  analogWrite(enB1,200);
  delay(100);
  digitalWrite(drill,HIGH);
  delay(100);
  digitalWrite(drill,LOW);
  delay(100);
  digitalWrite(in31,HIGH); //anticlockwise rotation
  digitalWrite(in41,LOW);
  delay(100);
  digitalWrite(in31,LOW); //off the collector
  digitalWrite(in41,LOW);
  delay(100);
  
}

void discc(){
  
  disc.write(90);
  delay(100); // extra required
  
}

void discBack(){

  disc.write(0);
  delay(100);
  
}

void messageForAcidic(){

  mySerial.println("AT+CMGF=1");    //Sets the GSM Module in Text Mode
  delay(100);  // Delay of 1000 milli seconds or 1 second
  mySerial.println("AT+CMGS=\"+919483703839\"\r"); 
  delay(100);
  mySerial.println("The field is basically acidic and it is preferred to grow potato or to grow other neutral based vegetables spray limestone");// The SMS text you want to send
  delay(100);
   mySerial.println((char)26);// ASCII code of CTRL+Z
  delay(100);
  
}

void messageForBasic(){

  mySerial.println("AT+CMGF=1");    //Sets the GSM Module in Text Mode
  delay(100);  // Delay of 1000 milli seconds or 1 second
  mySerial.println("AT+CMGS=\"+919483703839\"\r"); 
  delay(100);
  mySerial.println("The field is basically basic and it is preferred to grow all neutral based vegetables ");// The SMS text you want to send
  delay(100);
   mySerial.println((char)26);// ASCII code of CTRL+Z
  delay(100);
  
}

int GetNumber()
{
   int num = 0;
   char key = keypad.getKey();
   while(key != '#')
   {
      switch (key)
      {
         case NO_KEY:
            break;

         case '0': case '1': case '2': case '3': case '4':
         case '5': case '6': case '7': case '8': case '9':
            num = num * 10 + (key - '0');
            break;

         case '*':
            num = 0;
            Serial.println("noob");
            break;
      }

      key = keypad.getKey();
   }

   return num;
}

void loop() {
  
   Serial.println("Enter the length of the field");
   v1 = GetNumber();
   Serial.println(v1);
   Serial.println("Enter breadth of the field");
   v2= GetNumber();
   Serial.println(v2);
   v3 = sqrt((v1*v1)+(v2*v2));
   v4 = round(v3/30);
   Serial.println(v4);
  
  for(int i=1;i<=1;i++){
     Start();
     delay((v4)*300);
     Stop(); 
     delay(100); //common delay for each stop
     StartDriller();
     delay(500);
     StartCollector();
     delay(300);
    
  }
     waterPump();
     delay(100);
     roboticArmContract();
     delay(200);
     int sensorValue = analogRead(A0);
     float adjustor = sensorValue/100;
     Serial.println("The pH observed is :");
     Serial.println(adjustor);
     delay(100);
     roboticArmBack();
     delay(100);
            // This function is for next senosr(npk)
    /* discc();
     delay(100);
     roboticArm();  
     delay(100);
     discBack();
     delay(100);*/
      
  Serial.println("Round done");
  if(adjustor < 7){
    messageForAcidic();
    delay(100);
  }
  else{
    messageForBasic();
    delay(100);
  }
  Serial.println("Simulation completed");
  //in order to break out of the function and no nothing
  while(1) { }
 
}
